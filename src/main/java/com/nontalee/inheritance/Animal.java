/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.inheritance;

import java.awt.BorderLayout;

/**
 *
 * @author nonta
 */
public class Animal {

    protected String name;
    protected int numberofleg = 0;
    protected String color;

    public Animal(String name, String color, int numberofleg) {
        System.out.println("Animal created");
        this.name = name;
        this.color = color;
        this.numberofleg = numberofleg;

    }

    public void walk() {
        System.out.println("Animal walk");
    }

    public void speak() {
        System.out.println("Animal speak");
        System.out.println("name : " + this.name + " color : " + this.color
                + " numberofLegs : " + this.numberofleg);
    }

    public String getName() {
        return name;
    }

    public int getNumberofleg() {
        return numberofleg;
    }

    public String getColor() {
        return color;
    }

}

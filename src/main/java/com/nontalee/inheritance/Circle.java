/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.inheritance;

/**
 *
 * @author nonta
 */
public class Circle extends Shape {
    private double r;
    private double pi = 3.14;
    
    Circle(double r){
        this.r = r;
    }
    
    double calArea(){
        return getR()*getPi();
    }
    
    void print(){
        System.out.printf("Circle { r = "+getR()+" pi = 3.14} = %.2f",calArea());
    }

    public double getR() {
        return r;
    }

    public double getPi() {
        return pi;
    }
    
         
}

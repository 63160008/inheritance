/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.inheritance;

/**
 *
 * @author nonta
 */
public class TestShape {
    public static void main(String[] args) {
         Shape cir = new Circle(10.0);
         System.out.println(cir.calArea());
         
         Shape tri = new Triangle (5.0,3.0);
         System.out.println(tri.calArea());
         
         Shape rec = new Rectangle (2.0,2.0);
         System.out.println(rec.calArea());
         
         Shape sq = new Square(4);
         System.out.println(sq.calArea());
         System.out.println(sq instanceof Rectangle);
         System.out.println(sq instanceof Object);
    }
   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.inheritance;

/**
 *
 * @author nonta
 */
public class Rectangle extends Shape {
    private double widgth;
    private double height;
    
    Rectangle(double widgth,double height){
        this.widgth= widgth;
        this.height = height;
    }
    double calArea(){
        return getWidgth()*getHeight();
    }

    public double getWidgth() {
        return widgth;
    }

    public double getHeight() {
        return height;
    }
    
}

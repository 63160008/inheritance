/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.inheritance;

/**
 *
 * @author nonta
 */
public class Triangle extends Shape {

    private double base;
    private double height;

    Triangle(double base, double height) {
        this.base = base;
        this.height = height;

    }

    double calArea() {
        return (double)1 / 2 *  (getBase() * getHeight());
    }

    public double getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }

}
